// helper
const $ = s => document.querySelector(s)

function CounterListController(hostElement) {
  let appState = [{ counter: 0, weight: 1 }]

  const actions = {
    add: () => {
      appState = [...appState, { counter: 0, weight: 1 }]

      render()
    },
    remove: () => {
      if (appState.length === 0) {
        return
      }

      appState = appState.slice(0, -1)

      render()
    },
    clear: () => {
      appState = []

      render()
    },
    increment: index => {
      const { counter, weight } = appState[index]
      appState[index] = { ...appState[index], counter: counter + weight }

      render()
    },
    decrement: index => {
      const { counter, weight } = appState[index]
      appState[index] = { ...appState[index], counter: counter - weight }

      render()
    },
    reset: index => {
      appState[index] = { ...appState[index], counter: 0, weight: 1 }

      render()
    },
    updateWeight: (index, weight) => {
      appState[index] = { ...appState[index], weight }

      render()
    }
  }

  function render() {
    CounterListView(hostElement, {
      list: appState,
      onClickAdd: actions.add,
      onClickRemove: actions.remove,
      onClickClear: actions.clear,
      onChangeWeight: actions.updateWeight,
      onClickDecButton: actions.decrement,
      onClickIncButton: actions.increment,
      onClickResetButton: actions.reset
    }).render()
  }

  return { render }
}

function CounterListView(
  hostElement,
  {
    list,
    onClickAdd,
    onClickRemove,
    onClickClear,
    onClickDecButton,
    onClickIncButton,
    onClickResetButton,
    onChangeWeight
  }
) {
  const template = `
  <div class="btn-group ml-2">
    <button class="btn btn-primary" data-id="add-button">Add Counter</button>
    <button class="btn btn-danger" data-id="remove-button">Remove Counter</button>
    <button class="btn btn-secondary" data-id="clear-button">Clear</button>
  </div>
  <div data-id="list"></div>
`

  hostElement.innerHTML = template

  const addButtonElement = hostElement.querySelector('[data-id="add-button"]')
  const removeButtonElement = hostElement.querySelector(
    '[data-id="remove-button"]'
  )
  const clearButtonElement = hostElement.querySelector(
    '[data-id="clear-button"]'
  )
  const listElement = hostElement.querySelector('[data-id="list"]')

  addButtonElement.addEventListener('click', onClickAdd)
  removeButtonElement.addEventListener('click', onClickRemove)
  clearButtonElement.addEventListener('click', onClickClear)

  function render() {
    list.forEach(({ counter, weight }, index) => {
      const div = document.createElement('div')
      listElement.appendChild(div)

      CounterView(div, {
        counter,
        weight,
        onClickDecButton: onClickDecButton.bind(this, index),
        onClickIncButton: onClickIncButton.bind(this, index),
        onClickResetButton: onClickResetButton.bind(this, index),
        onChangeWeight: onChangeWeight.bind(this, index)
      }).render()
    })
  }

  return {
    render
  }
}

function CounterController(hostElement) {
  let appState = {
    counter: 0,
    weight: 1
  }

  const actions = {
    increment: () => {
      const { counter, weight } = appState
      appState = { ...appState, counter: counter + weight }

      render()
    },
    decrement: () => {
      const { counter, weight } = appState
      appState = { ...appState, counter: counter - weight }

      render()
    },
    reset: () => {
      appState = { ...appState, counter: 0, weight: 1 }

      render()
    },
    updateWeight: weight => {
      appState = { ...appState, weight }

      render()
    }
  }

  function render() {
    const view = CounterView(hostElement, {
      counter: appState.counter,
      weight: appState.weight,
      onClickIncButton: actions.increment.bind(this, 1),
      onClickDecButton: actions.decrement.bind(this, 1),
      onClickResetButton: actions.reset,
      onChangeWeight: actions.updateWeight
    })

    view.render()
  }

  return { render }
}

function CounterView(
  hostElement,
  {
    counter,
    weight,
    onClickIncButton,
    onClickDecButton,
    onClickResetButton,
    onChangeWeight
  }
) {
  const template = `
  <div class="d-flex justify-content-between align-items-center">
    <h1 class="m-0" data-id="counter">0</h1>
    <div class="d-inline-flex">
      <input data-id="weight" type="number" class="form-control" />
      <div class="btn-group ml-2">
        <button class="btn btn-primary" data-id="inc-button">+</button>
        <button class="btn btn-danger" data-id="dec-button">-</button>
        <button class="btn btn-secondary" data-id="reset-button">Reset</button>
      </div>
    </div>
  </div>
`

  hostElement.innerHTML = template

  const counterElement = hostElement.querySelector('[data-id="counter"]')
  const incButtonElement = hostElement.querySelector('[data-id="inc-button"]')
  const decButtonElement = hostElement.querySelector('[data-id="dec-button"]')
  const resetButtonElement = hostElement.querySelector(
    '[data-id="reset-button"]'
  )
  const weightInputElement = hostElement.querySelector('[data-id="weight"]')

  incButtonElement.addEventListener('click', onClickIncButton)
  decButtonElement.addEventListener('click', onClickDecButton)
  resetButtonElement.addEventListener('click', onClickResetButton)
  weightInputElement.addEventListener('change', event =>
    onChangeWeight(+event.target.value)
  )

  function render() {
    counterElement.textContent = counter
    weightInputElement.value = weight
  }

  return {
    render
  }
}

function appRender() {
  CounterListController($('#app')).render()
  CounterController($('#counter')).render()
}

appRender()